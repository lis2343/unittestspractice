import authenticationStatic.Authentication;
import authenticationStatic.CredentialsStaticService;
import authenticationStatic.PermissionStaticService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

public class AuthenticationStaticTest {
    private static Authentication authentication;
    private static MockedStatic<CredentialsStaticService> mockStaticCredentialService;
    private static MockedStatic<PermissionStaticService> mockStaticPermissionService;
    
    @BeforeAll()
    public static void setupMockitoObjects(){
        authentication = new Authentication();

        mockStaticCredentialService = Mockito.mockStatic(CredentialsStaticService.class);
        mockStaticCredentialService.when(()->CredentialsStaticService.isValidCredential("lisbeth.quisbert", "123456")).thenReturn(true);
        mockStaticCredentialService.when(()->CredentialsStaticService.isValidCredential("#sweet&butterfly", "sweet123")).thenReturn(false);
        mockStaticCredentialService.when(()->CredentialsStaticService.isValidCredential("pretty.doll@458", "pretty123")).thenReturn(false);
        mockStaticCredentialService.when(()->CredentialsStaticService.isValidCredential("google_was_my_idea.", "google678")).thenReturn(true);
        mockStaticCredentialService.when(()->CredentialsStaticService.isValidCredential("", "1234567890")).thenReturn(false);
        mockStaticCredentialService.when(()->CredentialsStaticService.isValidCredential("cute.as.ducks", "ducks999")).thenReturn(true);
        mockStaticCredentialService.when(()->CredentialsStaticService.isValidCredential("sweetie", "")).thenReturn(false);
        mockStaticCredentialService.when(()->CredentialsStaticService.isValidCredential("Dark Water Reflection", "darkwater")).thenReturn(true);
        mockStaticCredentialService.when(()->CredentialsStaticService.isValidCredential("Programmer", "progr456")).thenReturn(true);

        mockStaticPermissionService = Mockito.mockStatic(PermissionStaticService.class);
        mockStaticPermissionService.when(()->PermissionStaticService.getPermission("lisbeth.quisbert")).thenReturn("CRUD");
        mockStaticPermissionService.when(()->PermissionStaticService.getPermission("google_was_my_idea.")).thenReturn("CRU");
        mockStaticPermissionService.when(()->PermissionStaticService.getPermission("cute.as.ducks")).thenReturn("CR");
        mockStaticPermissionService.when(()->PermissionStaticService.getPermission("Dark Water Reflection")).thenReturn("R");
        mockStaticPermissionService.when(()->PermissionStaticService.getPermission("Programmer")).thenReturn("");
    }

    @AfterAll
    public static void closeMocks(){
        mockStaticCredentialService.close();
        mockStaticPermissionService.close();
    }

    @ParameterizedTest
    @CsvSource({
            "lisbeth.quisbert, 123456",
            "google_was_my_idea., google678",
            "cute.as.ducks, ducks999",
            "Dark Water Reflection, darkwater"
    })
    @DisplayName("Static - Verifies if a user and password are valid ones")
    public void verifyWhenUserAndPasswordAreCorrect(String username, String password){
        String actualResult = authentication.login(username, password);
        String expectedResult = "user authenticated successfully with permission: (\\[CRUD]|\\[CRU]|\\[CR]|\\[R])";
        Assertions.assertTrue(actualResult.matches(expectedResult));
    }

    @ParameterizedTest
    @CsvSource({
            "#sweet&butterfly, sweet123",
            "pretty.doll@458, pretty123"
    })
    @DisplayName("Static - Verifies when the user and password are not correct or empty")
    public void verifyWhenUserAndPasswordAreNotCorrect(String username, String password){
        String actualResult = authentication.login(username, password);
        String expectedResult = "user or password incorrect";
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Static - Verifies if the username is not sent")
    public void verifyIfUsernameIsNotSent(){
        String actualResult = authentication.login("", "1234567890");
        String expectedResult = "user or password incorrect";
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Static - Verifies if the password is not sent")
    public void verifyIfPasswordIsNotSent(){
        String actualResult = authentication.login("sweetie", "");
        String expectedResult = "user or password incorrect";
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    @DisplayName("Static - Verifies if the user does not have any permissions")
    public void verifyIfUserDoesNotHavePermissions(){
        String actualResult = authentication.login("Programmer", "progr456");
        String expectedResult = "user authenticated successfully with permission: []";
        Assertions.assertEquals(expectedResult, actualResult);
    }
}
