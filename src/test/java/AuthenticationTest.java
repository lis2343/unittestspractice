import authentication.Authentication;
import authentication.CredentialsService;
import authentication.PermissionService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

public class AuthenticationTest {

    private Authentication authentication;
    private CredentialsService mockCredentialService;
    PermissionService mockPermissionService;

    @BeforeEach()
    public void setupMockitoObjects(){
        mockCredentialService = Mockito.mock(CredentialsService.class);
        Mockito.when(mockCredentialService.isValidCredential("lisbeth.quisbert", "123456")).thenReturn(true);
        Mockito.when(mockCredentialService.isValidCredential("#sweet&butterfly", "sweet123")).thenReturn(false);
        Mockito.when(mockCredentialService.isValidCredential("pretty.doll@458", "pretty123")).thenReturn(false);
        Mockito.when(mockCredentialService.isValidCredential("google_was_my_idea.", "google678")).thenReturn(true);
        Mockito.when(mockCredentialService.isValidCredential("", "1234567890")).thenReturn(false);
        Mockito.when(mockCredentialService.isValidCredential("cute.as.ducks", "ducks999")).thenReturn(true);
        Mockito.when(mockCredentialService.isValidCredential("sweetie", "")).thenReturn(false);
        Mockito.when(mockCredentialService.isValidCredential("Dark Water Reflection", "darkwater")).thenReturn(true);
        Mockito.when(mockCredentialService.isValidCredential("Programmer", "progr456")).thenReturn(true);

        mockPermissionService = Mockito.mock(PermissionService.class);
        Mockito.when((mockPermissionService.getPermission("lisbeth.quisbert"))).thenReturn("CRUD");
        Mockito.when((mockPermissionService.getPermission("google_was_my_idea."))).thenReturn("CRU");
        Mockito.when((mockPermissionService.getPermission("cute.as.ducks"))).thenReturn("CR");
        Mockito.when((mockPermissionService.getPermission("Dark Water Reflection"))).thenReturn("R");
        Mockito.when((mockPermissionService.getPermission("Programmer"))).thenReturn("");

        authentication = new Authentication();
        authentication.setCredentialsService(mockCredentialService);
        authentication.setPermissionService(mockPermissionService);
    }

    @ParameterizedTest
    @CsvSource({
            "lisbeth.quisbert, 123456",
            "google_was_my_idea., google678",
            "cute.as.ducks, ducks999",
            "Dark Water Reflection, darkwater"
    })
    @DisplayName("Verifies if a user and password are valid ones")
    public void verifyWhenUserAndPasswordAreCorrect(String username, String password){
        String actualResult = authentication.login(username, password);
        String expectedResult = "user authenticated successfully with permission: (\\[CRUD]|\\[CRU]|\\[CR]|\\[R])";
        Assertions.assertTrue(actualResult.matches(expectedResult));

        switch(username) {
            case "lisbeth.quisbert":
                Mockito.verify(mockCredentialService).isValidCredential("lisbeth.quisbert", "123456");
                Mockito.verify(mockPermissionService).getPermission("lisbeth.quisbert");
                break;
            case "google_was_my_idea.":
                Mockito.verify(mockCredentialService).isValidCredential("google_was_my_idea.", "google678");
                Mockito.verify(mockPermissionService).getPermission("google_was_my_idea.");
                break;
            case "cute.as.ducks.":
                Mockito.verify(mockCredentialService).isValidCredential("cute.as.ducks", "ducks999");
                Mockito.verify(mockPermissionService).getPermission("cute.as.ducks");
                break;
            case "Dark Water Reflection":
                Mockito.verify(mockCredentialService).isValidCredential("Dark Water Reflection", "darkwater");
                Mockito.verify(mockPermissionService).getPermission("Dark Water Reflection");
                break;
            default:
                break;
        }
    }

    @ParameterizedTest
    @CsvSource({
            "#sweet&butterfly, sweet123",
            "pretty.doll@458, pretty123"
    })
    @DisplayName("Verifies when the user and password are not correct or empty")
    public void verifyWhenUserAndPasswordAreNotCorrect(String username, String password){
        String actualResult = authentication.login(username, password);
        String expectedResult = "user or password incorrect";
        Assertions.assertEquals(expectedResult, actualResult);

        switch(username) {
            case "#sweet&butterfly":
                Mockito.verify(mockCredentialService).isValidCredential("#sweet&butterfly", "sweet123");
                break;
            case "pretty.doll@458":
                Mockito.verify(mockCredentialService).isValidCredential("pretty.doll@458", "pretty123");
                break;
            default:
                break;
        }
    }

    @Test
    @DisplayName("Verifies if the username is not sent")
    public void verifyIfUsernameIsNotSent(){
        String actualResult = authentication.login("", "1234567890");
        String expectedResult = "user or password incorrect";
        Assertions.assertEquals(expectedResult, actualResult);

        Mockito.verify(mockCredentialService).isValidCredential("", "1234567890");
    }

    @Test
    @DisplayName("Verifies if the password is not sent")
    public void verifyIfPasswordIsNotSent(){
        String actualResult = authentication.login("sweetie", "");
        String expectedResult = "user or password incorrect";
        Assertions.assertEquals(expectedResult, actualResult);

        Mockito.verify(mockCredentialService).isValidCredential("sweetie", "");
    }

    @Test
    @DisplayName("Verifies if the user does not have any permissions")
    public void verifyIfUserDoesNotHavePermissions(){
        String actualResult = authentication.login("Programmer", "progr456");
        String expectedResult = "user authenticated successfully with permission: []";
        Assertions.assertEquals(expectedResult, actualResult);

        Mockito.verify(mockCredentialService).isValidCredential("Programmer", "progr456");
        Mockito.verify(mockPermissionService).getPermission("Programmer");
    }
}
